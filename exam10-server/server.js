const express = require('express');
const cors = require('cors');
const fileDb = require('./fileDb');
const post = require('./app/post');
const app = express();

const port = 8000;

app.use(cors());

app.use(express.json());
app.use(express.static('public'));

fileDb.init().then(() => {
    console.log('Database file was loaded!');

    app.use('/news', post(fileDb));

    app.listen(port, () => {
        console.log(`Server started on ${port} port!`);
    });
});