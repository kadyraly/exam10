const express = require('express');

const multer  = require('multer');
const path = require('path');
const nanoid = require('nanoid');
const config = require('../config');
const router = express.Router();
const storage = multer.diskStorage({
    destination: (req, file, cb) => {
    cb(null, config.uploadPath);
},
filename: (req, file, cb) => {

    cb(null, nanoid() + path.extname(file.originalname))
}
});
const upload = multer({storage});


const createRouter = (db) => {

    router.get('/', (req, res) => {
        res.send(db.getData(req.query.datetime));
});


    router.post('/', upload.single('image'), (req, res) => {

        const post = req.body;

    if (req.file) {
        post.image = req.file.filename;
    }


    db.addItem(post).then(result => {
        res.send(result);
});
});


    router.get('/:id', (req, res) => {
        res.send(db.getDataById(req.params.id));
});

    router.delete('/:id', (req, res) => {
        res.send(db.remove(req.params.id));
    });


    return router;
};

module.exports = createRouter;