import React, {Component, Fragment} from 'react';
import {Route, Switch} from "react-router-dom";


import Header from "./component/UI/Header/Header";
import News from "./container/News/News";
import NewPost from "./container/NewPost/NewPost";


class App extends Component {
    render() {
        return (
            <Fragment>
                <Header/>
                <main className="container">
                    <Switch>
                        <Route path="/" exact component={News}/>
                        <Route path="/news/new" exact component={NewPost}/>
                    </Switch>
                </main>
            </Fragment>
        );
    }
}

export default App
