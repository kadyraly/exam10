import  React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';


import NewsForm from "../../component/NewsForm/NewsForm";
import {createPost} from "../../store/action/post";


class NewPost extends Component {

    createPost = postData => {
        this.props.onPostCreated(postData).then(() => {
            this.props.history.push('/');
        });
    };
    render() {
        return (
            <Fragment>
                <NewsForm onSubmit={this.createPost} />
            </Fragment>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onPostCreated: postData => dispatch(createPost(postData))
    }
};

export default connect(null, mapDispatchToProps)(NewPost);