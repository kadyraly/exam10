import React, {Component} from 'react';
import {connect} from 'react-redux';
import {NavLink, Route} from "react-router-dom";
import './News.css';
import {deletePost, fetchPosts} from "../../store/action/post";

class News extends Component {

    componentDidMount() {
        this.props.onFetchPosts();
    }

    deletePost = id => {
        this.props.onPostDeleted(id).then(() => {

        });
    };
    render() {
        return (
            <div className="News">
                <h1>News</h1>
                {this.props.posts.map(post => (
                    <div className="news" key={post.id}>
                        {post.image?
                            <img style={{width: '100px', marginRight: '10px'}}
                                 src={'http://localhost:8000/uploads/' + post.image}/> : null}
                        <h3>{post.title}</h3>
                        <NavLink exact  to='/more'>Read Full Post</NavLink>
                        <Route path='/' render={() => {
                            return (
                                <p> {post.content}</p>
                            )
                        }} />
                        <button onClick={()=>this.deletePost(1)}>delete</button>

                    </div>
                ))}
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        posts: state.posts.posts
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onFetchPosts: () => dispatch(fetchPosts()),
        onPostDeleted: id => dispatch(deletePost(id))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(News);