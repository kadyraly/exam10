import axios from '../../axios-api';
import {CREATE_POST_SUCCESS, DELETE_POST_SUCCESS, FETCH_POSTS_SUCCESS} from "./actionTypes";



export const fetchPostsSuccess = posts => {
    return {type: FETCH_POSTS_SUCCESS, posts};
};

export const fetchPosts = () => {
    return dispatch => {
        axios.get('/news').then(
            response => dispatch(fetchPostsSuccess(response.data))
        );

    };
};

export const createPostSuccess = () => {
    return {type: CREATE_POST_SUCCESS};
};

export const createPost = postData => {
    return dispatch => {
        return axios.post('/news', postData).then(
            response => dispatch(createPostSuccess())
        );
    };
};
export const deletePostSuccess = () => {
    return {type: DELETE_POST_SUCCESS};
};


export const deletePost = id => {
    return dispatch => {
        return axios.delete('/news/' + id).then(
            response => dispatch(deletePostSuccess())
        );
    };
};