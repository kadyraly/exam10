import React, {Component} from 'react';
import './NewsForm.css';

class NewsForm extends Component {
    state = {
        title: '',
        content: '',
        image: ''
    };

    submitFormHandler = event => {
        event.preventDefault();
        const formData = new FormData();
        Object.keys(this.state).forEach(key => {
            formData.append(key, this.state[key]);
        });

        this.props.onSubmit(formData);
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    fileChangeHandler = event => {

        this.setState({
            [event.target.name]: event.target.files[0]
        });
    };

    render() {
        return (
            <form action="" className="NewsForm" onSubmit={this.submitFormHandler}>
                <div>
                    <label>Title:</label>
                    <input
                        type="text"
                        value={this.state.title}
                        placeholder="title here..."
                        className="form-control"
                        name="title"
                        onChange={this.inputChangeHandler}
                    />
                </div>
                <div>
                    <label>Content:</label>
                    <textarea
                        name="content"
                        value={this.state.content}
                        placeholder="content"
                        className="form-control"
                        onChange ={this.inputChangeHandler}
                        required
                    />
                </div>
                <div>
                    <label>Image:</label>
                    <input
                        type="file"
                        name="image"
                        onChange ={this.fileChangeHandler}
                    />
                </div>

                <div>
                    <button type="submit">Send</button>
                </div>
            </form>
        );
    }
}

export default NewsForm;
