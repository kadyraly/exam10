import React from 'react';
import './Header.css';
import {NavLink} from "react-router-dom";

const Header = props => {
    return (
        <div className="Header">
            <ul>
                <li><NavLink activeClassName="selected" exact to="/">News</NavLink></li>
                <li><NavLink activeClassName="selected" to="/news/new">New Post</NavLink></li>
            </ul>
        </div>
    )
};

export  default Header;